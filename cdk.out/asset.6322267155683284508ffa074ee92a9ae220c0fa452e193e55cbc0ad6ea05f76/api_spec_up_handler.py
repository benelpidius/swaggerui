import boto3
import logging
import os
from botocore.exceptions import ClientError
import json

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_json():
    """
    Gets the swagger-ui JSON of an API gateway which is in . 
    Returns the JSON object in string format.
    """
    client = boto3.client(
        'apigateway',
        region_name='us-east-1',
    )

    try:
        # Get the file from the API gateway by exporting
        response = client.get_export(
            restApiId='dhajylbsm9', # api gateway id
            stageName='prod',
            exportType='swagger',
            parameters={
                'extensions':'apigateway'
            },
            accepts='application/json'
        )

        if 'body' not in response:
            raise ValueError("No documentation body received.")

        spec = response['body'].read().decode('utf-8') 
    except ClientError as e:
        logging.error(e)
        return False
    
    return spec
        
def upload_file(file):
    """Upload a file in object format to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    s3 = boto3.client('s3')
    s3_bucket_name = 'swaggerui-stateful-s3apidocscb10f954-1gdoxw29pmji2'
    key_name = 'auris.json'

    try:
        s3.put_object(Body=file, Bucket=s3_bucket_name, Key=key_name)
        logger.info("Success.")

    except ClientError as e:
        logging.error(e)
        logger.info("Fail.")
        return False
    
    return True

def handler(event, context):   
    file_obj = get_json()
    logger.info(f'File object: {file_obj}')

    s3 = upload_file(file_obj)
    
    return s3 

