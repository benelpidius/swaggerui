import boto3
import logging
import os
from botocore.exceptions import ClientError

def get_json():
    """
    Gets the JSON file of an API gateway. 
    Returns the JSON file in object format.
    """
    client = boto3.client(
        'apigateway',
        region_name='us-east-1',
    )

    response = client.get_export(
        restApiId='hxgie18hrj',
        stageName='prod',
        exportType='swagger',
        parameters={
            'extensions':'apigateway'
        },
        accepts='application/json'
    )

    if 'body' not in response:
        raise ValueError("No documentation body received.")

    spec = response['body'].read().decode('utf-8')
    logging(spec)

    return spec
        
def upload_file(file):
    """Upload a file in object format to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # Upload the file
    client = boto3.client('s3')

    try:
        with open(file, 'rb') as data:
            client.upload_fileobj(data, 'swaggerui-stateful-s3apidocscb10f954-1gdoxw29pmji2', 'auris.json')

    except ClientError as e:
        logging.error(e)
        return False
    return True

def handler(event, context):
    file_obj = get_json()

    upload_file(file_obj)

