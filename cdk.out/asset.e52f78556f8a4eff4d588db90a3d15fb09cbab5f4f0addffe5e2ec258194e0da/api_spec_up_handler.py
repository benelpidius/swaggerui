import boto3
import logging
import os
from botocore.exceptions import ClientError
import json

def get_json():
    """
    Gets the JSON file of an API gateway. 
    Returns the JSON file in object format.
    """
    client = boto3.client(
        'apigateway',
        region_name='us-east-1',
    )

    try:
        response = client.get_export(
            restApiId='hxgie18hrj',
            stageName='prod',
            exportType='swagger',
            parameters={
                'extensions':'apigateway'
            },
            accepts='application/json'
        )

        if 'body' not in response:
            raise ValueError("No documentation body received.")

        spec = response['body'].read().decode('utf-8')
        logging.info(spec)
    except ClientError as e:
        logging.error(e)
        return False
    
    return spec
        
def upload_file(file):
    """Upload a file in object format to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    s3 = boto3.client('s3')
    s3_bucket_name = 'swaggerui-stateful-s3apidocscb10f954-1gdoxw29pmji2'
    key_name = 'auris.json'

    try:
        with open(json.dumps(file), 'rb') as swagger_json:
            s3.upload_fileobj(swagger_json, s3_bucket_name, key_name)

    except ClientError as e:
        logging.error(e)
        return False
    
    return True

def handler(event, context):   
    file_obj = get_json()
    s3 = upload_file(file_obj)
    
    return s3 

