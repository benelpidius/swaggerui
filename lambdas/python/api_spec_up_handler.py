import boto3
import logging
import os
from botocore.exceptions import ClientError
import json

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_json():
    """
    Gets the swagger-ui JSON of an API gateway.
    Returns the swagger-ui JSON in string format.
    """

    client = boto3.client(
        'apigateway',
        region_name='us-east-1',
    )

    try:
        # Get the file from the API gateway by exporting
        response = client.get_export(
            restApiId='dhajylbsm9', # api gateway id -> to be changed
            stageName='prod',
            exportType='swagger',
            parameters={
                'extensions':'apigateway'
            },
            accepts='application/json'
        )

        if 'body' not in response:
            raise ValueError("No documentation body received.")

        spec = response['body'].read().decode('utf-8') # converts the body key which is of binary type to string type
    except ClientError as e:
        logger.error(e)
        
        return False
    
    return spec
        
def upload_file(file):
    """
    Uploads a string in object format to an S3 bucket
    Returns True if the string was uploaded, else False
    """

    s3 = boto3.client('s3')
    s3_bucket_name = 'swaggerui-stateful-s3apidocscb10f954-1gdoxw29pmji2'
    key_name = 'auris.json' # name of the file to be given

    try:
        # Uploads the string to the s3 bucket 
        s3.put_object(Body=file, Bucket=s3_bucket_name, Key=key_name)
        logger.info("Success.")
    except ClientError as e:
        logger.info("Fail.")
        logger.error(e)

        return False
    
    return True

def handler(event, context):   
    json = get_json()
    logger.info(f'JSON Object: {json}')

    s3 = upload_file(json)
    
    return s3 
